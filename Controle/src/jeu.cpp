#include "jeu.h"
#include<string>

Jeu::Jeu(std::string nomJeu, int pourcentChancesVictoire)
{
    this->nom = nomJeu;
    this->pourcentChances = pourcentChancesVictoire;
    //ctor
}

Jeu::~Jeu()
{
    //dtor
}

std::string Jeu::getNom()
{
    return this->nom;
}

int Jeu::getPourcentChances()
{
    return this->pourcentChances;
}
