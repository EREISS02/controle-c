#include "casino.h"
#include <fstream>
#include <string>
#include <vector>
#include "Jeu.h"

Casino::Casino()
{
    //ctor
}

Casino::~Casino()
{
    //dtor
}

void Casino::sauvegarde(std::string nomJoueur, int nbJetonsJoueurs) //sauvegarde dans le fichier texte les donn�es du joueur
{
    std::ofstream fichier("Sauvegarde.txt", std::ios::out);

    fichier << nomJoueur << std::endl;
    fichier << nbJetonsJoueurs;

    fichier.close();
}

std::vector<std::string> Casino::chargeSave() //charge les donn�es du fichier texte
{
    std::ifstream fichier("Sauvegarde.txt", std::ios::in);

    std::vector<std::string> donnees;
    std::string ligne;

    while(getline(fichier, ligne))
    {
        donnees.push_back(ligne);
    }

    return donnees;
}

std::vector<Jeu> Casino::creationJeux()
{
    //cr�ation des jeux
    Jeu *blackJack = new Jeu("BlackJack", 40); //pourcentage de chances totalement arbitraire
    Jeu *roulette = new Jeu("Roulette", 49);
    Jeu *craps = new Jeu("Craps", 45);

    std::vector<Jeu> liste;

    //introduction des jeux dans un vector
    liste.push_back(*blackJack);
    liste.push_back(*roulette);
    liste.push_back(*craps);

    return liste;
}


