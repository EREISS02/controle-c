#include "joueur.h"
#include <string>

Joueur::Joueur()
{
    //ctor
}

Joueur::~Joueur()
{
    //dtor
}

std::string Joueur::getNom()
{
    return this->nom;
}

int Joueur::getJetons()
{
    return this->jetons;
}

void Joueur::setJetons(int val)
{
    this->jetons = val;
}

void Joueur::setNom(std::string val)
{
    this->nom = val;
}

void Joueur::miser(int mise)
{
    this->jetons = this->jetons - mise;
}

void Joueur::doubleSaMise(int mise)
{
    this->jetons = this->jetons + mise *2;
}

void Joueur::setChoixJeu(int jeu)
{
    this->choixJeu = jeu;
}

int Joueur::getChoixJeu()
{
    return this->choixJeu;
}

int Joueur::getMise()
{
    return this->mise;
}

void Joueur::setMise(int miseJoueur)
{
    this->mise = miseJoueur;
}
