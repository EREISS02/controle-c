#include <iostream>
#include <fstream>
#include <time.h>
#include <stdlib.h>
#include <vector>
#include <string>
#include "casino.h"
#include "joueur.h"
#include "Jeu.h"


using namespace std;

int main()
{
    srand(time(NULL));
    const int nbJetonsDepart = 100; //nombre de jetons de d�part pour chaque nouveau joueur
    int sauvegarde;
    Casino *partie = new Casino();
    Joueur *user = new Joueur();
    string nomJoueur;
    vector<string> save = partie->chargeSave();
    bool jouer = true;

    cout << "Charger votre partie : 1\nCommencez une nouvelle partie : 2"<<endl;

    do
    {
        cin >> sauvegarde;
    }
    while (sauvegarde < 1 || sauvegarde > 2); //tant que l'utilisateur donne un nombre diff�rent de 1 ou 2

    if(sauvegarde == 1) //si l'utilisateur a choisi de charger sa sauvegarde
    {
        user->setNom(save[0]);
        user->setJetons(atoi(save[1].c_str()));
        cout << user->getNom() << endl;
        cout << user->getJetons() << endl;
    }
    else //si l'utilisateur a choisi de commencer une nouvelle partie
    {
        cout << "Entrez votre nom" << endl;
        cin >> nomJoueur;
        user->setNom(nomJoueur);
        user->setJetons(nbJetonsDepart);
    }

    partie->sauvegarde(user->getNom(), user->getJetons()); //�crase la sauvegarde pr�c�dente

    vector<Jeu> listeJeux = partie->creationJeux();

    do
    {
        cout << "Vous avez " << user->getJetons() << " jetons." << endl;
        cout << "A quel jeu souhaitez-vous jouer ?" << endl;

        for(unsigned int i = 0; i < listeJeux.size(); i++) //affiche la liste des jeux disponibles
        {
            cout << listeJeux[i].getNom() << " : " << i+1 <<  endl;
        }
        int choix;
        cin >> choix;
        user->setChoixJeu(choix);

        cout << "Vous avez choisi le " << listeJeux[user->getChoixJeu()-1].getNom() << endl; //affiche le jeu que l'utilisateur a choisi
        cout << "Combien de jetons souhaitez-vous miser ?" << endl;

        int mise;
        cin >> mise;
        user->setMise(mise);
        //Le joueur Joue
        if(rand()%100+1 <= listeJeux[user->getChoixJeu()-1].getPourcentChances()) //savoir si l'utilisateur a gagn�
        {
            cout << "Vous doublez votre mise" << endl;
            user->miser(user->getMise());
            user->doubleSaMise(user->getMise());
        }
        else
        {
            cout << "Vous perdez votre mise" << endl;
            user->miser(user->getMise());
        }

        //La partie est termin�e on demande au joueur s'il souhaite rejouer, sauvegarder (rejouer ou quitter), charger ou quitter
        int choixFinPartie;

        do
        {
            cout << "Souhaitez-vous :\nRejouer (1)\nSauvegarder et rejouer (2)\nSauvegarder et quitter (3)\nCharger la sauvegarde pr�c�dente (4)\nQuitter (5)" << endl;
            cin >> choixFinPartie;
        }
        while (choixFinPartie < 1 || choixFinPartie > 5);

        switch(choixFinPartie)
        {
        case 1: //rejoue sans sauvegarder
            break;
        case 2: //rejoue et sauvegarde
            partie->sauvegarde(user->getNom(), user->getJetons());
            save = partie->chargeSave();
            break;
        case 3: //sauvegarde et quitte
            partie->sauvegarde(user->getNom(), user->getJetons());
            jouer = false;
            break;
        case 4: //charge la sauvegarde pr�c�dente
            user->setNom(save[0]);
            user->setJetons(atoi(save[1].c_str()));
            break;
        case 5: //quitte le jeu
            jouer = false;
            break;
        }
    }
    while(jouer == true); //tant que le joueur souhaite jouer il reste dans le casino

    delete user;
    return 0;
}
