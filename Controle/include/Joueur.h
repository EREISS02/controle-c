#ifndef JOUEUR_H
#define JOUEUR_H
#include <string>


class Joueur
{
public:
    Joueur();
    virtual ~Joueur();

    int getJetons();
    void setJetons(int val);
    void setNom(std::string val);
    std::string getNom();
    void doubleSaMise(int val);
    void miser(int val);
    void setChoixJeu(int val);
    int getChoixJeu();
    int getMise();
    void setMise(int miseJoueur);
protected:

private:
    int jetons;
    std::string nom;
    int choixJeu;
    int mise;
};

#endif // JOUEUR_H
