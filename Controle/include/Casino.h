#ifndef CASINO_H
#define CASINO_H
#include <string>
#include <vector>
#include "Jeu.h"

class Casino
{
public:
    Casino();
    virtual ~Casino();
    void sauvegarde(std::string nomJoueur, int nbJetonsJoueurs);
    std::vector<std::string> chargeSave();
    std::vector<Jeu> creationJeux();
protected:

private:
};

#endif // CASINO_H
