#ifndef JEU_H
#define JEU_H
#include<string>


class Jeu
{
public:
    Jeu(std::string nomJeu, int pourcentChancesVictoire);
    virtual ~Jeu();

    std::string getNom();
    int getPourcentChances();
protected:

private:
    std::string nom;
    int pourcentChances;
};

#endif // JEU_H
